/*******************************
         Galette Overrides
*******************************/

/*--------------
     Header
---------------*/

.ui.menu .header.item,
.ui.vertical.menu .header.item {
  font-weight: @headerWeight;
  text-align: left;
}

& when (@variationMenuFixed) {
  /*--------------
       Fixed
  ---------------*/
  .ui.fixed.menu .item {
    background: rgba(255,255,255,.6);
  }
  .ui.fixed.menu .item.active {
    background: @activeItemBackground;
  }
}

& when (@variationMenuVertical) {
  /*--------------
      Vertical
  ---------------*/

  .ui.vertical.menu {
    display: block;
    flex-direction: column;
    background: @verticalBackground;
    box-shadow: @verticalBoxShadow;
  }
  .ui.vertical.menu {
    border: @border;
  }
  .ui.vertical.menu div.item .ui.button {
    width: 100%;
    border: @border;
    margin: 0 0 .4rem 0;
    box-shadow: none;
  }
  .ui.vertical.menu div.item .item.button::before {
    content: none;
  }
  .ui.vertical.accordion.menu {
    a.active.item,
    a.item:hover {
      & > .title {
        color: @hoveredTextColor;
      }
    }
  }
}

/* --------------
      Hover
--------------- */

.ui.link.menu .item:hover,
.ui.menu .dropdown.item:hover,
.ui.menu .link.item:hover,
.ui.menu a.item:hover {
    color: @hoveredTextColor;
}

/*------------------
     Pagination
-------------------*/
@media only screen and (max-width: 991px) {
  .ui.menu.pagination {
    width: 100%;
  }
}

/*----------------------------------
     Compact mode logout menu
-----------------------------------*/
.compact_menu #logoutmenu {
  & > a.item {
    &.darkmode {
      background-color: @darkmodeItemBackground;
      &:hover,
      &:focus {
        background-color: @darkmodeHover;
      }
      &:active {
        background-color: @darkmodeActive;
      }
      &.black {
        background-color: @darkmodeItemBackgroundInverted;
        color: @invertedTextColor;
        &:hover,
        &:focus {
          background-color: @blackHover;
        }
        &:active {
          background-color: @blackDown;
        }
      }
    }
    &.purple {
      background-color: @logoutItemBackgroundImpersonated;
      color: @invertedTextColor;
      &:hover,
      &:focus {
        background-color: @purpleHover;
      }
      &:active {
        background-color: @purpleDown;
      }
    }
    &.red {
      background-color: @logoutItemBackground;
      color: @invertedTextColor;
      &:hover,
      &:focus {
        background-color: @redHover;
      }
      &:active {
        background-color: @redDown;
      }
    }
  }
}
